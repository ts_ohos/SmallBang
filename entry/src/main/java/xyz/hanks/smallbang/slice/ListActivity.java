package xyz.hanks.smallbang.slice;

import xyz.hanks.smallbang.ListBean;
import xyz.hanks.smallbang.ResourceTable;
import xyz.hanks.smallbang.adapters.ListAdapter;
import java.util.ArrayList;
import java.util.List;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;

public class ListActivity extends AbilitySlice {
  ListContainer listContainer;

  @Override
  protected void onStart(Intent intent) {
    super.onStart(intent);
    setUIContent(ResourceTable.Layout_ability_list);
    listContainer = (ListContainer) findComponentById(ResourceTable.Id_list_view);
    List<ListBean> data = new ArrayList<>();
    for (int i = 0; i < 100; i++) {
      ListBean item = new ListBean();
      item.setLiked(false);
      item.setTitle("this is content : " + i);
      data.add(item);
    }
    listContainer.setItemProvider(new ListAdapter(this, data));
  }
}
