package xyz.hanks.smallbang.slice;

import xyz.hanks.library.bang.OnAnimationEndListener;
import xyz.hanks.library.bang.SmallBangView;
import xyz.hanks.smallbang.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.window.dialog.ToastDialog;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class MainAbilitySlice extends AbilitySlice implements OnAnimationEndListener {
  private static final HiLogLabel LABEL_LOG = new HiLogLabel(HiLog.LOG_APP, 0x1234,
      "MainAbilitySlice");
  //LikeButton starButton;
  SmallBangView smallBangView;
  SmallBangView heart;
  Image button;
  private ToastDialog dialog;

  @Override
  public void onStart(Intent intent) {
    super.onStart(intent);
    super.setUIContent(ResourceTable.Layout_ability_main);
    heart = (SmallBangView) findComponentById(ResourceTable.Id_heart);
    smallBangView = (SmallBangView) findComponentById(ResourceTable.Id_heart_button);

    button = (Image) findComponentById(ResourceTable.Id_button);

    smallBangView.setOnAnimationEndListener(this);
    heart.setClickedListener(new Component.ClickedListener() {
      @Override
      public void onClick(Component component) {
        if (heart.isLiked()) {
          heart.setLiked(false);
          dialog.setText("").show();
        } else {
          heart.setLiked(true);
          dialog.setText("heart+1").show();
        }
      }
    });
    smallBangView.setClickedListener(new Component.ClickedListener() {
      @Override
      public void onClick(Component component) {
        if (smallBangView.isLiked()) {
          smallBangView.setLiked(false);
        } else {
          smallBangView.setLiked(true);
        }
      }
    });

    button.setClickedListener(component -> {
      present(new ListActivity(), new Intent());

    });

    dialog = new ToastDialog(this);
  }

  @Override
  public void onAnimationEnd(SmallBangView smallBangView) {
    HiLog.info(LABEL_LOG, "Animation End for ");
  }

  @Override
  public void onActive() {
    super.onActive();
  }

  @Override
  public void onForeground(Intent intent) {
    super.onForeground(intent);
  }
}
