# SmallBang

本项目是基于开源项目SmallBang进行harmonyos化的移植和开发的，可以通过项目标签以及
[github地址](https://github.com/hanks-zyh/SmallBang)

移植版本：源master v1.2.2 版本

## 项目介绍
### 项目名称：SmallBang
### 所属系列：harmonyos的第三方组件适配移植
### 功能：
     展示爱心动画。

### 项目移植状态：完全移植
### 调用差异：基本没有使用差异，请参照demo使用
### 原项目Doc地址：https://github.com/hanks-zyh/SmallBang
### 编程语言：java

### 项目截图（涉及文件仅供demo测试使用）

![demo运行效果](art/art.png)
![运行效果](art/demo.gif)

## 安装教程

#### 方案一  
 
可以先下载项目，将项目中的smallbang-library库提取出来放在所需项目中通过build配置
```Java
dependencies {
     implementation project(":smallbang-library")
}
```

#### 方案二

- 1.项目根目录的build.gradle中的repositories添加：
```groovy
    buildscript {
       repositories {
           ...
           mavenCentral()
       }
       ...
   }
   
   allprojects {
       repositories {
           ...
           mavenCentral()
       }
   }
```
- 2.开发者在自己的项目中添加依赖
```groovy
dependencies {
    implementation 'com.gitee.ts_ohos:SmallBang:1.0.0'
}
```

# How to use
    
       
       <xyz.hanks.library.bang.SmallBangView
            ohos:height="match_content"
            ohos:width="match_content"
            app:circle_end_color="$color:sheng_hong"
            app:circle_start_color="$color:fen_hong"
            app:dots_primary_color="$color:fen_hong"
            app:dots_secondary_color="$color:sheng_hong"
            app:icon_show="true"
            app:icon_size="20vp"
            app:like_drawable="$media:heart_on"
            app:unlike_drawable="$media:heart_off"
            />
自定义属性 app:circle_end_color="$color:sheng_hong"
            app:circle_start_color="$color:fen_hong"
            app:dots_primary_color="$color:fen_hong"
            app:dots_secondary_color="$color:sheng_hong"是设置颜色的，
自定义属性  app:icon_show="true" 设置true显示图片,false 显示文字。

自定义属性 app:icon_size="20vp" 设置显示图片的大小。

自定义属性 app:like_drawable="$media:heart_on"
            app:unlike_drawable="$media:heart_off" 设置图片变化前后.

<xyz.hanks.library.bang.SmallBangView
            ohos:id="$+id:heart_button"
            ohos:height="match_content"
            ohos:width="match_content"
            ohos:layout_alignment="horizontal_center"
            app:circle_end_color="$color:sheng_hong"
            app:circle_start_color="$color:fen_hong"
            app:dots_primary_color="$color:fen_hong"
            app:dots_secondary_color="$color:sheng_hong"
            app:icon_show="false"
            app:like_drawable="$media:heart_on"
            app:text_size="50"
            app:text_text="hanks"
            app:unlike_drawable="$media:heart_off"
            />
自定义属性  app:text_size="50"
            app:text_text="hanks" 设置文字和文字大小
     
## License

This library is licensed under the [Apache Software License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0).

See [`LICENSE`](LICENSE) for full of the license text.

    Copyright (C) 2015 [Hanks](https://github.com/hanks-zyh)

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.