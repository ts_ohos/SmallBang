package xyz.hanks.library.bang;


import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Paint;
import ohos.agp.render.BlendMode;
import ohos.agp.render.Canvas;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.Texture;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

/**
 *Created by hanks
 */

public class CircleView extends Component implements Component.DrawTask, Component.EstimateSizeListener,
    Component.BindStateChangedListener {
  private static final String TAG = "renqing";
  private int START_COLOR = 0xFFFF5722;
  private int END_COLOR = 0xFFFFC107;

  private Paint circlePaint = new Paint();
  private Paint maskPaint = new Paint();

  private PixelMapElement tempBitmap;
  private Canvas tempCanvas;

  private float outerCircleRadiusProgress = 0f;
  private float innerCircleRadiusProgress = 0f;

  private int width = 0;
  private int height = 0;

  private int maxCircleSize;

  public CircleView(Context context) {
    super(context);
    init();
  }

  public CircleView(Context context, AttrSet attrs) {
    super(context, attrs);
    init();
  }

  public CircleView(Context context, AttrSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    init();
  }


  private void init() {
    addDrawTask(this);
    setEstimateSizeListener(this);
    setBindStateChangedListener(this);
    circlePaint.setStyle(Paint.Style.FILL_STYLE);
    circlePaint.setAntiAlias(true);
    maskPaint.setBlendMode(BlendMode.CLEAR);
    maskPaint.setAntiAlias(true);
  }

  public void setSize(int width, int height) {
    this.width = width;
    this.height = height;
    invalidate();
  }


  @Override
  public boolean onEstimateSize(int i, int i1) {
    int wMode = EstimateSpec.getMode(i);
    int hMode = EstimateSpec.getMode(i1);
    if (width != 0 && height != 0) {
      setEstimatedSize(Component.EstimateSpec.getSizeWithMode(width, wMode),
          Component.EstimateSpec.getSizeWithMode(height, hMode));
      return true;
    }
    return false;
  }


  @Override
  public void onDraw(Component component, Canvas canvas) {
    tempCanvas.drawColor(0xffffff, BlendMode.CLEAR);
    tempCanvas.drawCircle(getWidth() / 2, getHeight() / 2, outerCircleRadiusProgress
        * maxCircleSize, circlePaint);
    tempCanvas.drawCircle(getWidth() / 2, getHeight() / 2, innerCircleRadiusProgress
        * maxCircleSize + 1, maskPaint);
    PixelMapHolder hoder = new PixelMapHolder(tempBitmap.getPixelMap());
    canvas.drawPixelMapHolder(hoder, 0, 0, new Paint());
  }

  public void setInnerCircleRadiusProgress(float innerCircleRadiusProgress) {
    this.innerCircleRadiusProgress = innerCircleRadiusProgress;
    invalidate();
  }

  public float getInnerCircleRadiusProgress() {
    return innerCircleRadiusProgress;
  }

  public void setOuterCircleRadiusProgress(float outerCircleRadiusProgress) {
    this.outerCircleRadiusProgress = outerCircleRadiusProgress;
    updateCircleColor();
    invalidate();
  }

  private void updateCircleColor() {
    this.circlePaint.setColor(new Color(START_COLOR));
  }

  public float getOuterCircleRadiusProgress() {
    return outerCircleRadiusProgress;
  }


  public void setStartColor(int color) {
    START_COLOR = color;
    invalidate();
  }

  public void setEndColor(int color) {
    END_COLOR = color;
    invalidate();
  }

  @Override
  public void onComponentBoundToWindow(Component component) {
    maxCircleSize = getWidth() / 2;
    PixelMap.InitializationOptions ops = new PixelMap.InitializationOptions();
    ops.editable = true;
    ops.pixelFormat = PixelFormat.ARGB_8888;
    ops.size = new Size(getWidth(), getWidth());
    PixelMap pixelMap = PixelMap.create(ops);
    tempBitmap = new PixelMapElement(pixelMap);

    Texture texture = new Texture(pixelMap);
    tempCanvas = new Canvas(texture);
  }

  @Override
  public void onComponentUnboundFromWindow(Component component) {

  }
}