/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package xyz.hanks.library.bang;


/**
 *
 */
public class Icon {
  private int onIconResourceId;
  private int offIconResourceId;
  private IconType iconType;

  public Icon(int onIconResourceId, int offIconResourceId, IconType iconType) {
    this.onIconResourceId = onIconResourceId;
    this.offIconResourceId = offIconResourceId;
    this.iconType = iconType;
  }

  public int getOffIconResourceId() {
    return offIconResourceId;
  }

  public void setOffIconResourceId(int offIconResourceId) {
    this.offIconResourceId = offIconResourceId;
  }

  public int getOnIconResourceId() {
    return onIconResourceId;
  }

  public void setOnIconResourceId(int onIconResourceId) {
    this.onIconResourceId = onIconResourceId;
  }

  public IconType getIconType() {
    return iconType;
  }

  public void setIconType(IconType iconType) {
    this.iconType = iconType;
  }
}
