package xyz.hanks.library.bang;


import xyz.hanks.library.bang.ResourceTable;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Texture;
import ohos.agp.utils.Rect;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Miroslaw Stanek on 21.12.2015.
 */
public class Utils {
  public static double mapValueFromRangeToRange(double value, double fromLow, double fromHigh, double toLow, double toHigh) {
    return toLow + ((value - fromLow) / (fromHigh - fromLow) * (toHigh - toLow));
  }

  public static double clamp(double value, double low, double high) {
    return Math.min(Math.max(value, low), high);
  }

  public static List<Icon> getIcons() {
    List<Icon> icons = new ArrayList<>();
    icons.add(new Icon(ResourceTable.Media_heart_on, ResourceTable.Media_heart_off, IconType.Heart));
    icons.add(new Icon(ResourceTable.Media_star_on, ResourceTable.Media_star_off, IconType.Star));
    icons.add(new Icon(ResourceTable.Media_thumb_on, ResourceTable.Media_thumb_off, IconType.Thumb));

    return icons;
  }

  public static PixelMap resizeDrawable(Context context, PixelMap drawable, int width, int height) {
    return getBitmap(new PixelMapElement(drawable), width, height);
  }


  private static PixelMap getBitmap(PixelMapElement vectorDrawable, int width, int height) {
    PixelMap.InitializationOptions ops = new PixelMap.InitializationOptions();
    ops.size = new Size(width, height);
    ops.editable = true;
    PixelMap bitmap = PixelMap.create(ops);
    Texture tus = new Texture(bitmap);
    Canvas canvas = new Canvas(tus);
    Rect localClipBounds = canvas.getLocalClipBounds();
    vectorDrawable.setBounds(0, 0, localClipBounds.right, localClipBounds.bottom);
    vectorDrawable.drawToCanvas(canvas);
    return bitmap;
  }

  public static float dipToPixels(Context context, float dipValue) {
    return AttrHelper.vp2px(dipValue, context);
  }
}

